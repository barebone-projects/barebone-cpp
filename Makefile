cxx=c++

target-dir=./target/
src-dir=./src/
obj-dir=$(target-dir)obj/

srcs=$(wildcard $(src-dir)*)
objs=$(addsuffix .o,$(addprefix $(obj-dir),$(notdir $(srcs))))

target=$(target-dir)target-name

CXX_FLAGS=-std=c++17
LD_FLAGS=

all: $(target)

run: $(target)
	@$(target)

$(target): $(objs)
	$(cxx) -o $@ $^ $(CXX_FLAGS) $(LD_FLAGS)

$(obj-dir)%.cpp.o: $(src-dir)%.cpp
	$(cxx) -c -o $@ $^ $(CXX_FLAGS)

clean:
	rm -rf $(target) $(obj-dir)*
